config ATH5KP
	tristate "Atheros 5xxx wireless cards support (for 802.11p)"
	depends on (PCI || ATHEROS_AR231X) && MAC80211
	select ATH_COMMON
	select MAC80211_LEDS
	select LEDS_CLASS
	select NEW_LEDS
	select AVERAGE
	select ATH5K_AHB if (ATHEROS_AR231X && !PCI)
	select ATH5K_PCI if (!ATHEROS_AR231X && PCI)
	---help---
	  (802.11p compliant)
	  This module adds support for wireless adapters based on
	  Atheros 5xxx chipset.

	  Currently the following chip versions are supported:

	  MAC: AR5211 AR5212
	  PHY: RF5111/2111 RF5112/2112 RF5413/2413

	  This driver uses the kernel's mac80211 subsystem.

	  If you choose to build a module, it'll be called ath5kp. Say M if
	  unsure.

config ATH5KP_DEBUG
	bool "Atheros 5xxx debugging (for 802.11p)"
	depends on ATH5KP
	---help---
	  (802.11p compliant)
	  Atheros 5xxx debugging messages.

	  Say Y, if and you will get debug options for ath5kp.
	  To use this, you need to mount debugfs:

	  mount -t debugfs debug /sys/kernel/debug

	  You will get access to files under:
	  /sys/kernel/debug/ath5kp/phy0/

	  To enable debug, pass the debug level to the debug module
	  parameter. For example:

	  modprobe ath5kp debug=0x00000400

config ATH5KP_TRACER
	bool "Atheros 5xxx tracer (for 802.11p)"
	depends on ATH5KP
	depends on EVENT_TRACING
	---help---
	  (802.11p compliant)
	  Atheros 5xxx debugging messages.
	  Say Y here to enable tracepoints for the ath5kp driver
	  using the kernel tracing infrastructure.  Select this
	  option if you are interested in debugging the driver.

	  If unsure, say N.

config ATH5KP_AHB
	bool "Atheros 5xxx AHB bus support (for 802.11p)"
	depends on (ATHEROS_AR231X && !PCI)
	---help---
	  (802.11p compliant)
	  Atheros 5xxx debugging messages.
	  This adds support for WiSoC type chipsets of the 5xxx Atheros
	  family.

config ATH5KP_PCI
	bool "Atheros 5xxx PCI bus support (for 802.11p)"
	depends on (!ATHEROS_AR231X && PCI)
	---help---
	  (802.11p compliant)
	  Atheros 5xxx debugging messages.
	  This adds support for PCI type chipsets of the 5xxx Atheros
	  family.

config ATH5KP_TEST_CHANNELS
	bool "Enables testing channels on ath5k (for 802.11p)"
	depends on ATH5KP && CFG80211_CERTIFICATION_ONUS
	---help---
	  (802.11p compliant)
	  Atheros 5xxx debugging messages.
	  This enables non-standard IEEE 802.11 channels on ath5kp, which
	  can be used for research purposes. This option should be disabled
	  unless doing research.
