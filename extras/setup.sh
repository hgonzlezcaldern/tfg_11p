#!/bin/bash
oldIFS=$IFS
IFS=$'\n'

for line in $(cat codMod/codMod.txt)
do
 echo $line
 file=$(echo $line | cut -f1 -d' ')
 dir=$(echo $line | cut -f2 -d' ')
 echo $file 
 echo $dir 
 cp "$dir" "$dir".orig 
 cp codMod/"$file" "$dir"
done


IFS=$oldIFS
