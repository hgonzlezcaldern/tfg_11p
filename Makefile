ath5kp-y				+= caps.o
ath5kp-y				+= initvals.o
ath5kp-y				+= eeprom.o
ath5kp-y				+= gpio.o
ath5kp-y				+= desc.o
ath5kp-y				+= dma.o
ath5kp-y				+= qcu.o
ath5kp-y				+= pcu.o
ath5kp-y				+= phy.o
ath5kp-y				+= reset.o
ath5kp-y				+= attach.o
ath5kp-y				+= base.o
CFLAGS_base.o			+= -I$(src)
ath5kp-y				+= led.o
ath5kp-y				+= rfkill.o
ath5kp-y				+= ani.o
ath5kp-y				+= sysfs.o
ath5kp-y				+= mac80211-ops.o
ath5kp-$(CONFIG_ATH5KP_DEBUG)	+= debug.o
ath5kp-$(CONFIG_ATH5KP_AHB)	+= ahb.o
ath5kp-$(CONFIG_ATH5KP_PCI)	+= pci.o
obj-$(CONFIG_ATH5K)		+= ath5kp.o
